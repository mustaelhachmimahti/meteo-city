const fetchData = async () => {
  const cityName = document.getElementById('city').value;
  if (cityName.length) {
    const data = await fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=16af1e9a74f42f4a64ac7726d3974e48&units=metric`
    );
    const res = await data.json();
    const content = document.getElementById('test');
    const loader = document.getElementById('loader');
    const searchResults = document.getElementById('searchResults');
    content.innerHTML = `
  <div class="d-flex align-items-center row">
    <img
      src='http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png'
      class="col-md-3 rounded-circle mt-5 img-fluid"
      alt="Temperature"
    />
    <div class="col mt-5">
      <p>${res.main.temp}°</p>
      <p>${res.main.temp_max}°</p>
      <p>${res.main.temp_min}°</p>
      <p>${res.main.feels_like} <i class="fas fa-thumbs-up"></i></p>
    </div>
  </div>
  
  <h2 class="text-center">${res.name}</h2>
  
  <div id="hum-speed">
    <img
      src="./img/viento.jpeg"
      class="mt-5 img-fluid"
      alt="Temperature"
      id="status-image"
    />
    <img
      src="./img/humed.jpg"
      class="mt-5 img-fluid"
      alt="Temperature"
      id="status-image"
    />
  </div>
  <div id="hum-speed">
    <p>${res.main.humidity} <i class="fas fa-tint"></i></p>
    <p>${res.wind.speed} <i class="fas fa-wind"></i></p>
    </div>`;
    searchResults.style.display = 'none';
  } else {
    const content = document.getElementById('test');
    content.innerHTML = `
<div><p>No data</p></div>`;
  }
};

document.getElementById('get-data').addEventListener('click', fetchData);

const searchCityLive = async e => {
  const searchResults = document.getElementById('searchResults');
  if (e.length === 0) {
    searchResults.style.display = 'none';
  }
  if (e.length > 2) {
    searchResults.style.display = 'block';
    try {
      const data = await fetch(
        `https://api-adresse.data.gouv.fr/search/?q=${e}&autocomplete=0`
      );
      const res = await data.json();
      if (res < 1) {
        searchResults.innerHTML += '<p>Film no found</p>';
      }
      searchResults.innerHTML = '';
      res.features.forEach((e, i) => {
        i > 0
          ? (searchResults.innerHTML += '<hr>')
          : (searchResults.innerHTML = '');
        searchResults.innerHTML += `<p class="city">${e.properties.city}</p>`;
      });

      const city = [...document.querySelectorAll('.city')];
      city.map(ele =>
        ele.addEventListener('click', function () {
          document.getElementById('city').value = ele.innerHTML;
          document.getElementById('get-data').click();
        })
      );
    } catch (e) {}
  }
};

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    const content = document.getElementById('test');
    content.innerHTML = `
<div><p>No data</p></div>`;
  }
}

async function showPosition(position) {
  const data = await fetch(
    `https://api.openweathermap.org/data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&appid=16af1e9a74f42f4a64ac7726d3974e48&units=metric`
  );
  const res = await data.json();
  const content = document.getElementById('test');
  content.innerHTML = `
  <div class="d-flex align-items-center row">
    <img
      src='http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png'
      class="col-md-3 rounded-circle mt-5 img-fluid"
      alt="Temperature"
    />
    <div class="col mt-5">
      <p>${res.main.temp}°</p>
      <p>${res.main.temp_max}°</p>
      <p>${res.main.temp_min}°</p>
      <p>${res.main.feels_like} <i class="fas fa-thumbs-up"></i></p>
    </div>
  </div>
  
  <h2 class="text-center">${res.name}</h2>
  
  <div id="hum-speed">
    <img
      src="./img/viento.jpeg"
      class="mt-5 img-fluid"
      alt="Temperature"
      id="status-image"
    />
    <img
      src="./img/humed.jpg"
      class="mt-5 img-fluid"
      alt="Temperature"
      id="status-image"
    />
  </div>
  <div id="hum-speed">
    <p>${res.main.humidity} <i class="fas fa-tint"></i></p>
    <p>${res.wind.speed} <i class="fas fa-wind"></i></p>
    </div>`;
}

getLocation();
